﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Threading.Tasks;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.Core.Domain;
using Otus.Teaching.PromoCodeFactory.Core.Domain.Administration;

namespace Otus.Teaching.PromoCodeFactory.DataAccess.Repositories
{
    public class InMemoryRepository<T>
        : IRepository<T>
        where T: BaseEntity
    {
        private List<string> exceptProp = new List<string>() { "Id", "FullName", "Roles" };

        protected IEnumerable<T> Data { get; set; }

        public InMemoryRepository(IEnumerable<T> data)
        {
            Data = data;
        }

        public Task<IEnumerable<T>> GetAllAsync()
        {
            return Task.FromResult(Data);
        }

        public Task<T> GetByIdAsync(Guid id)
        {
            return Task.FromResult(Data.FirstOrDefault(x => x.Id == id));
        }

        public Task<bool> CreateAsync(T entity)
        {
            bool success = false;

            if (entity != null)
            {
                ((List<T>)Data).Add(entity);
                success = true;
            }
            return Task.FromResult(success);
        }

        public Task<bool> UpdateAsync(T entity)
        {
            bool success = false;
            var item = ((List<T>)Data).FirstOrDefault(x => x.Id == entity.Id);

            if (item != null)
            {
                CopyProps(entity, item);
                success = true;
            }
            return Task.FromResult(success);
        }

        public Task<bool> DeleteByIdAsync(Guid id)
        {
            bool success = false;

            var item = ((List<T>)Data).FirstOrDefault(x => x.Id == id);

            if (item != null)
            {
                ((List<T>)Data).Remove(item);
                success = true;
            }

            return Task.FromResult(success);
        }

        private void CopyProps(T source, T target)
        {
            foreach (var prop in source.GetType().GetProperties())
            {
                if (!exceptProp.Contains(prop.Name))
                {
                    var value = prop.GetValue(source, null);
                    target.GetType().GetProperties().FirstOrDefault(p => p.Name == prop.Name).SetValue(target, value);
                }
            }

        }
    }
}