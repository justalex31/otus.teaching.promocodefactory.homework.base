﻿using AutoMapper;
using Otus.Teaching.PromoCodeFactory.Core.Domain.Administration;

namespace Otus.Teaching.PromoCodeFactory.WebHost.Mapping
{
    public class MappingProfile : Profile
    {
        public MappingProfile()
        {
            CreateMap<Models.Employee.EmployeeCreateOrEditModel, Employee>();
            CreateMap<Employee, Models.Employee.EmployeeCreateOrEditModel>();
            CreateMap<Models.RoleItemResponse, Role>();
            CreateMap<Role, Models.RoleItemResponse>();
        }
    }

}
